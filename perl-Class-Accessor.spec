%global _empty_manifest_terminate_build 0
Name:           perl-Class-Accessor
Version:        0.51
Release:        2
Summary:        Automated accessor generation
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
Group:          Development/Libraries
URL:            https://search.cpan.org/dist/Class-Accessor/
Source0:        https://www.cpan.org/authors/id/K/KA/KASEI/Class-Accessor-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker)

%description
This module automagically generates accessors/mutators for your class.

%package help
Summary : Automated accessor generation
Provides: perl-Class-Accessor-doc

%description help
This module automagically generates accessors/mutators for your class.

%prep
%setup -q -n Class-Accessor-%{version}

%build
export PERL_MM_OPT=""
%{__perl} Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT

%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%doc Changes META.json README
%{perl_vendorlib}/*

%files help
%{_mandir}/man?/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 0.51-2
- cleanup spec

* Wed Aug 26 2020 wangyue <wangyue92@huawei.com> 0.51-1
- package init
